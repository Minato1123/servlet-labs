/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.servletlabs;

/**
 *
 * @author carrie
 */
public class Account {
    private String id = "";
    private String password = "";
    private String email = "";
    private String mobile = "";
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
        // 只寫 id 會找最近的變數，加上 this 指定
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    
    
    
}
