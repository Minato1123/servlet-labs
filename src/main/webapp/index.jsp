<%@page import="com.mycompany.servletlabs.Account"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                height: 96vh;
            }
            .form-block {
                box-sizing: border-box;
                padding: 2rem;
                background-color: #c1dec2;
                border-radius: 1rem;
                border: 0.1rem solid #669669;
            }
            
            a {
                color: white;
                padding: 0.3rem 1rem;
                display: inline-block;
                background-color: #77a179;
                border-radius: 1rem;
                margin-bottom: 1rem;
                transition: all 0.2s;
                text-decoration: none;
            }
            
            a:hover {
                background-color: #647d65;
            }
            
            form {
                display: flex;
                flex-direction: column;
                justify-content: space-between;
                gap: 0.8rem;
                width: 16rem;
            }
            
            form > div {
                display: flex;
                justify-content: space-between;
                height: 1.6rem;
            }
        </style>
    </head>
    <body>
        <div class="form-block">
            <a href="clearServlet">Clear</a>
            <form method="post" action="addAccountServlet">
                <%
                    Account account = (Account) session.getAttribute("account");
                    if ( account == null ) {
                        account = new Account();
                    }
                %>
                <div>Id: <input name="id" value="<%= account.getId() %>" /></div>
                <div>Password: <input name="password" type="password" value="<%= account.getPassword() %>" /></div>
                <div>Email: <input name="email" value="<%= account.getEmail() %>" /></div>
                <div>Mobile: <input name="mobile" value="<%= account.getMobile() %>" /></div>
                <input type="submit" />
            </form>
        </div>
    </body>
</html>
