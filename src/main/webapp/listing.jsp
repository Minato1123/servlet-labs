<%-- 
    Document   : listing
    Created on : 2022年11月22日, 下午4:13:52
    Author     : carrie
--%>

<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            li {
                padding: 0.3rem 0;
                font-size: 1.2rem;
            }
        </style>
    </head>
    <body>
        <%
            File folder = new File(application.getRealPath("/uploads"));
            File[] files = folder.listFiles();
        %>
        
        <ul>
            
        <%
            for (File file : files) {
                if (!file.getName().matches(".DS_Store")) {
        %>
        
            <li><a href="download?file=<%= file.getName() %>"> [Download] </a><a href="uploads/<%= file.getName() %>" target="_blank"> <%= file.getName() %></a></li>
        
        <%
                }
            }
        %>
        
        </ul>
    </body>
</html>
